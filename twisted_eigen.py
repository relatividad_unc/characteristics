#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 22:08:29 2019

@author: reula
"""

from sympy import *
import sympy.functions as sp
from sympy.plotting import plot3d_parametric_line, plot3d, plot3d_parametric_surface
import numpy as np
import matplotlib.pyplot as mpl
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm, colors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import animation, rc
from IPython.display import HTML
x, y, z, k1, k2, k3, s, a, b, c, spp, spm = symbols('x y z k1 k2 k3 s a b c, spp, spm')
#k, m, n = symbols('k m n', integer=True)
theta = Symbol("theta")
phi = Symbol("phi")

K=Matrix([[0,k1,0,0],[k1,0,k2,0],[0,k2,0,k3],[0,0,k3,0]])

print(K)

(P, D) = K.diagonalize()

print(P)

print(D.subs(k3,1/sqrt(2)).subs(k1,cos(phi)/sqrt(2)).subs(k2,sin(phi)/sqrt(2)))

print(simplify(D.subs(k3,1/sqrt(2)).subs(k1,cos(phi)/sqrt(2))[1,1]).subs(k2,sin(phi)/sqrt(2)))

#PP = P.subs(k3,1/sp.sqrt(2)).subs(k1,sp.cos(phi)/sp.sqrt(2)).subs(k2,sp.sin(phi)/sp.sqrt(2))

#T = PP*PP.inv().transpose()

#print('T=',simplify(T[1,1]))

U = Matrix([[-k1,-k1,-k1,-k1],[spp, spm, -spp, -spm],\
           [-spp**2*k2/(spp**2 - k3**2),-spm**2*k2/(spm**2 - k3**2),-spp**2*k2/(spp**2 - k3**2),-spm**2/(spm**2 - k3**2)],\
           [spp*k2*k3/(spp**2 - k3**2),spm*k2*k3/(spm**2 - k3**2),-spp*k2*k3/(spp**2 - k3**2),-spm*k2*k3/(spm**2 - k3**2)]])

print(U)

Uinv = U.inv()
print(simplify(Uinv.subs(spp, sp.sqrt((1+sp.sqrt(1+4*k1**2*k3**2))/2)).\
      subs(spm, sp.sqrt((1-sp.sqrt(1+4*k1**2*k3**2))/2))))


