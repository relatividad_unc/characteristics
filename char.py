# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from sympy import *
import sympy.functions as sp
from sympy.plotting import plot3d_parametric_line
import numpy as np
import matplotlib.pyplot as mpl
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm, colors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import animation, rc
from IPython.display import HTML
x, y, z, k1, k2, s, a, b, c, d = symbols('x y z k1 k2 s a b c d')
#k, m, n = symbols('k m n', integer=True)
theta = Symbol("theta")
phi = Symbol("phi")

wave = False
twisted_wave = True
# this is the smallest stronlgy hyperbilic system (a, A)
AA = False 
# generic 
BB = False
BC = False

K0=Matrix([[1,0,0],[0,1,0],[0,0,1]])

if wave:
    K1 = Matrix([[0,1,0],[1,0,0],[0,0,0]])
    K2 = Matrix([[0,0,0],[0,0,1],[0,1,0]])
    
if twisted_wave:
    K1 = Matrix([[0,1,0],[1,0,0],[0,0,0]])
    K2 = Matrix([[0,0,0],[0,0,-1],[0,-1,0]])
    
if AA:
    K1 = Matrix([[0,1,1],[1,0,0],[-a,0,0]])
    K2 = Matrix([[0,-1,1],[a,0,0],[1,0,0]])
    
if BB:
    K1 = Matrix([[0,b,-a],[b,0,1],[-a,1,0]])
    K2 = Matrix([[0,d,c],[d,0,1],[c,1,1]])
    
if BC:
    K1 = Matrix([[1,0,0],[0,0,0],[0,0,0]])
    K2 = Matrix([[0,0,0],[0,1,0],[0,0,0]])
    
T = K0*s + K1*k1 + K2*k2

print(T)

print(T.det())

R=solve(T.det(),s,rational=None)


Initial = True
One_crossing = False
Touching = False
Good_double = False
Double_crossing = False


if Initial:
    A=0.
    B=1.
    C=0.
    D=0.
    
if One_crossing:
    A=0.9
    B=3.95
    C=0.8
    D=0.9

if Touching:
    A=1.
    B=0.0
    C=-1.
    D=0.
    
if Good_double:
    A=1.
    B=2.0
    C=-1.
    D=-2.

if Double_crossing:
    A=1.
    B=2.0
    C=-1.
    D=1.


L=2.
p0 = R[0].subs(a,A).subs(b,B).subs(c,C).subs(d,D).subs(k1,cos(x)).subs(k2,sin(x))
p1 = R[1].subs(a,A).subs(b,B).subs(c,C).subs(d,D).subs(k1,cos(x)).subs(k2,sin(x))
p2 = R[2].subs(a,A).subs(b,B).subs(c,C).subs(d,D).subs(k1,cos(x)).subs(k2,sin(x))


p = plot((p0,(x, 0., L*pi)),(p1,(x, 0., L*pi)),(p2,(x, 0., L*pi)))

p[0].line_color = 'b'
p[1].line_color = 'r'
p[2].line_color = 'g'

p.show()
#p.save('char_p9_p95_0p5.png')

norm0 = sqrt(p0**2 + 1.) 
norm1 = sqrt(p1**2 + 1.)
norm2 = sqrt(p2**2 + 1.)

n0z = p0/norm0
n0x = cos(x)/norm0
n0y = sin(x)/norm0
n1z = p1/norm1
n1x = cos(x)/norm1
n1y = sin(x)/norm1
n2z = p2/norm2
n2x = cos(x)/norm2
n2y = sin(x)/norm2

#plot1 = plot3d_parametric_line((n1x,n1y,n1z, (x, 0., L*pi)),(n2x,n2y,n2z, (x, 0., L*pi)))
plot3 = plot3d_parametric_line((n0x,n0y,n0z, (x, 0., L*pi)),(n1x,n1y,n1z, (x, 0., L*pi)),(n2x,n2y,n2z, (x, 0., L*pi)))
plot3.xlim = (-1.,1.)
plot3.ylim = (-1.,1.)
plot3.zlim = (-1.,1.)
plot3.nb_of_points = 10000

plot3[0].line_color = 'b'
plot3[1].line_color = 'r'
plot3[2].line_color = 'g'
plot3.autoscale=True
plot3.aspect_ratio = (-1.,1.)

plot3.show()

